const http = require("http");
const { app } = require("./app/app");

const httpServer = http.createServer(app);
const link = process.env.BASE_URL;
const port = process.env.PORT;
const protocol = process.env.PROTOCOL;
const base_url = `${protocol}://${link}:${port}`;

httpServer.listen(port, () => {
  console.log(`Server Running ${base_url}`);
});
