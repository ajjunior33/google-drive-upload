const mongoose = require("mongoose");

const DriveSchema = new mongoose.Schema({
  name: {
    require: true,
    type: String,
  },
  idFile: {
    require: true,
    type: String,
  },
  idOwner: {
    require: true,
    type: String,
    default: "Anonymous",
  },
});

module.exports = mongoose.model("Drive", DriveSchema);
