const { Router } = require("express");
const driveRouter = require("./drive.routes");

const route = Router();

route.get("/", (request, response) => {
  try {
    return response.status(200).json({
      name: process.env.APP_NAME,
      protocol: process.env.PROTOCOL,
      author: {
        name: process.env.AUTHOR_NAME,
        email: process.env.AUTHOR_EMAIL,
      },
      port: process.env.PORT,
      link: process.env.BASE_URL,
      baseUrl: `${process.env.PROTOCOL}://${process.env.BASE_URL}:${process.env.PORT}`,
      baseUpload: process.env.DIR_UPLOAD,
    });
  } catch (error) {
    return response.status(400).json({
      message: error.message,
    });
  }
});

route.use("/upload/drive", driveRouter);

module.exports = {
  route,
};
