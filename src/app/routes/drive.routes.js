const { Router } = require("express");
const multer = require("multer");
const DriverController = require("../controller/DriverController");
const multerConfig = require("../config/multerConfig");
const upload = multer(multerConfig);

const driveRouter = Router();

driveRouter
  .get("/", DriverController.index)
  .post("/", upload.single("file"), DriverController.upload)
  .delete("/:id", DriverController.destroy)
  .get('/:fileId', DriverController.show)
  .get('/test/:fileId', DriverController.copy);

module.exports = driveRouter;
