const drive = require("../modules/auth_google");
const Drive = require("../models/Drive");
const fs = require("fs");
const path = require("path");

module.exports = {
  async index(request, response) {
    try {
      const listFiles = await Drive.find();

      return response.status(200).json({
        message: "Lista de arquivos",
        listFiles,
        status: true,
      });
    } catch (error) {
      return response.status(400).json({
        message: error.message || "Não foi possível listar os arquivos.",
        status: false,
      });
    }
  },
  async copy(request, response) {
    const { fileId } = request.params;
    try {
      const result = await drive.files.copy({
        fileId: fileId,
      });

      console.log(result.data);
    } catch (error) {
      return response.status(400).json({
        message: error.message,
        status: false,
      });
    }
  },
  async show(request, response) {
    try {
      const { fileId } = request.params;
      await drive.permissions.create({
        fileId,
        requestBody: {
          role: "reader",
          type: "anyone",
        },
      });

      const result = await drive.files.get({
        fileId,
        fields: "webViewLink, webContentLink",
      });

      return response.status(200).json({
        message: "Dados do arquivo",
        data: result.data,
        status: true,
      });
    } catch (error) {
      return response.status(400).json({
        message: error.message,
        status: false,
      });
    }
  },
  async upload(request, response) {
    try {
      const { mimetype, filename } = request.file;
      const { name } = request.body;
      const typeFile = mimetype.split("/")[1];
      const nameFile = `${name}.${typeFile}`;
      const filePath = path.resolve(__dirname, "..", "uploads", `${filename}`);
      const uploadDrive = await drive.files.create({
        requestBody: {
          name: nameFile,
          mimeType: mimetype,
        },
        media: {
          mimeType: mimetype,
          body: fs.createReadStream(filePath),
        },
      });

      if (uploadDrive.status !== 200) {
        throw new Error("Houve um error ao subir sua imagem");
      }

      const saveBD = await Drive.create({
        name: nameFile,
        idOwner: "André Souza",
        idFile: uploadDrive.data.id,
      });

      if (!saveBD) {
        throw new Error("Houve um erro ao salvar no banco de dados.");
      }

      return response.status(200).json({
        message: "Enviado para Google Drive",
        idFile: uploadDrive.data.id,
        status: true,
      });
    } catch (error) {
      return response.status(400).json({
        message: error.message,
        status: false,
      });
    }
  },

  async destroy(request, response) {
    try {
      const { id } = request.params;
      const verifyId = await Drive.findById(id);

      if (!verifyId) {
        throw new Error("Não encontrei o ID selecionado.");
      }

      const fileId = verifyId.idFile;
      const deleteDrive = await drive.files.delete({
        fileId,
      });

      if (deleteDrive.status !== 204) {
        throw new Error("Houve um erro ao deletar o arquivo.");
      }

      const deleteFile = await Drive.findByIdAndDelete(id);

      if(!deleteFile){
        throw new Error('Não foi possível apagar o arquivo do banco de dados.');
      }

      return response.status(200).json({
        message: "Deletado com sucesso",
        status: true,
      });
    } catch (error) {
      return response.status(400).json({
        message: error.message,
        status: false,
      });
    }
  },
};
