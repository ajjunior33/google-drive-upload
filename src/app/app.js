require("dotenv").config();
const express = require("express");
const { route } = require("./routes/routes");
const mongoose = require("mongoose");
const app = express();

mongoose.connect("mongodb://localhost:27017/googleDriveApiSandbox?retryWrites=true&w=majority",{
    useUnifiedTopology:true,
    useNewUrlParser:true
});

app.use(express.json());
app.use(route);

module.exports = {
  app,
};
