require('dotenv').config();
const { google } = require('googleapis');
const path = require('path');
const fs = require("fs");

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;

const REFRESH_TOKEN = process.env.REFRESH_TOKEN;

const oauth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
);

oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });
const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
});

const filePath = path.join(__dirname, 'images', 'girl.jpg');

async function uploadFile() {
    try {
        const response = await drive.files.create({
            requestBody: {
                name: 'beautifulgirl.jpg',
                mimeType: 'image/jpg', 
            },
            media: {
                mimeType: 'image/jpg',
                body: fs.createReadStream(filePath)
            }
        });

        console.log(response.data);
    } catch (error) {
        console.log(error.message);
    }
}

async function deleteFile(id) {
    try {
        const response = await drive.files.delete({
            fileId: id
        })
        console.log(response.data, response.status);

    } catch (error) {
        console.log(error.message);
    }
}

async function generatePublicUrl(fileId) {
    try {
        await drive.permissions.create({
            fileId,
            requestBody: {
                role: 'reader',
                type: 'anyone'
            }
        });

        const result = await drive.files.get({
            fileId,
            fields: 'webViewLink, webContentLink',
        });
        console.log(result.data);
    } catch (error) {
        console.log(error.message);
    }
}

async function getImages() { //TO-DO
    try {
        const result = await drive.files.list(
            {
                q: "mimeType='application/vnd.google-apps.folder'",
                fields: 'nextPageToken, files(id, name)',
                spaces:'drive',
                pageToken: "1D0b2-mnmyctGXvSowAy6zb4MpgUz4I7w"
            }
        );
        console.log(result.data.files);
    } catch (error) {
        console.log(error.message);
    }
}



// uploadFile();
// deleteFile('1cIi2PLIHTkIkxM2lPwnhjHor_ZtORO0F');
// generatePublicUrl('1ZeLYD6y5JB-lPx1VVO4Ghl1yDiri_-b3');
getImages();