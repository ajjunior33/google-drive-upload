# Autenticação
Para se autenticar no Google Drive adicione as váriaveis de ambiente: 

    - CLIENT_ID
    - CLIENT_SECRET
    - REDIRECT_URI
    - REFRESH_TOKEN

Funcionalidades com comentário **TO-DO** estão sendo desenvolvidas ainda.


# Observações


### Upload de arquivos temporarios

Na pasta ```src/``` você deve criar uma pasta chamada ```uploads/```, mas ela vai ser ignorada pelo git. Também deve ter uma rotina que esvazie essa pasta. Uma outra solução e fazer a alteração do arquivo ```multerConfig.js``` para que os arquivos caiam em uma pasta temporaria.